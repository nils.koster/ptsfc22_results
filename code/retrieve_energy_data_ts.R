#setwd("/home/johannes/Documents/Teaching/Ensemble_Seminar2/ptsfc22_results/code")

library(rjson)
library(lubridate)

# avoid e notation in characters
options(scipen = 100)

# load hourly data:
library(rjson)
file_timestamps <- "https://www.smard.de/app/chart_data/410/DE/index_hour.json"
data_timestamps <- fromJSON(paste(readLines(file_timestamps), collapse=""))
data_timestamps <- as.data.frame(data_timestamps)
data_timestamps$time <- as.POSIXct(data_timestamps$timestamps/1000, origin="1970-01-01")
# timestamps to read in one year of data:
latest_timestamps <- as.character(tail(data_timestamps$timestamps, 26))

df <- NULL

for(tst in latest_timestamps){
  file_data <- paste0("https://www.smard.de/app/chart_data/410/DE/410_DE_hour_",
                      tst, ".json")
  data <- fromJSON(paste(readLines(file_data), collapse=""))
  
  timestamps <- unlist(sapply(data$series, FUN = function(x) x[[1]]))
  value <- unlist(sapply(data$series, FUN = function(x) x[[2]]))/1000
  timestamps <- timestamps[seq_along(value)]
  time <- as.POSIXct(timestamps/1000, origin="1970-01-01")
  
  df_temp <- data.frame(time = time, value = value)
  df_temp <- subset(df_temp, hour(df_temp$time) %% 4 == 0)
  
  if(is.null(df)){
    df <- df_temp
  }else{
    df <- rbind(df, df_temp)
  }
}

plot(df$time, df$value, type = "l")



write.csv(df, file = "../ptsfc22_viz/plot_data/energy.csv", row.names = FALSE)
list.dirs("..")
